# root/locals.tf

locals {
  keys = {
    mosar-test-1 = {
      public_key_path = "./public_keys/mosar_server_ssh.pub"
      key_name        = "mosar_server_ssh1"
    }
  }
}


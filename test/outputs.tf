output "key-pairs" {
  value = module.key-pairs
  sensitive = true
}

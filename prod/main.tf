# root/main.tf

module "key-pairs" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-key-pairs.git?ref=tags/v1.0.1"
  keys        = local.keys
  environment = var.environment
  managed_by  = "key-pairs"
}


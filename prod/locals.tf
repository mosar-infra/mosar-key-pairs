# root/locals.tf

locals {
  keys = {
    mosar-server = {
      public_key_path = "./public_keys/mosar-prod.pub"
      key_name        = "mosar_server_ssh_${var.environment}"
    }
  }
}

